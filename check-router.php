<?php
/**
 * Is called with a query string like this: ?querystring
 * This query string contains the mac of the router.
 * The script checks whether the time limit of the registration
 * of the router has expired or not. Returns 'OK' or 'NOK'.
 */

//get the registration data from the query string
$mac = $_SERVER["QUERY_STRING"];
$mac = rawurldecode($mac);

//include the framework classes and utilities
include_once "webapp.php";

//check that this router is not already registered
$query = "SELECT mac FROM routers WHERE mac = '$mac'";
$rs = WebApp::execQuery($query);
if ($rs->EOF())
{
  //the router is not in the list of the clients, so it is unregistered
  $reply = 'NOK';
}
else
{
  //the router is in the list, it is still registered
  $reply = 'OK';
}

//print the reply
print $reply;

//log the event
//$ip = $_SERVER['REMOTE_ADDR'];
//$details = ("Source=check-router, MAC=$mac, IP=$ip, Reply=$reply");
//log_event('CHECK', $details);
?>
#!/bin/bash
### init the DB tables of the application

### go to this directory
cd $(dirname $0)

### get the variables
. vars

### create the database
echo "create database $database;" \
  | mysql --host=$host --user=$user --password=$passwd 

### create the tables
mysql --host=$host --user=$user --password=$passwd \
      --database=$database < database.sql

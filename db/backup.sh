#!/bin/bash
### create a backup file and compress it (.gz)

if [ "$1" = "" ]
then
  echo "Usage: $0 fname"
  exit 1
fi

fname=$1

### go to this directory
cd $(dirname $0)

### get the variables
. vars

### backup the database into a file
mysqldump --add-drop-table --allow-keyword --complete-insert \
          --host=$host --user=$user --password=$passwd \
          --databases $database > backup/$fname

### compress
rm -rf backup/$fname.gz
gzip backup/$fname


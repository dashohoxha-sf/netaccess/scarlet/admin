#!/bin/bash
### dump the DB tables of the module

### go to this directory
cd $(dirname $0)

### get the variables
. vars

### dump the database into the file dbname.sql
mysqldump --add-drop-table --allow-keyword \
          --complete-insert --extended-insert \
          --host=$host --user=$user --password=$passwd \
          --databases $database > database.sql

### dump the database structure into the file dbname-tables.sql
mysqldump --add-drop-table --allow-keyword --no-data \
          --host=$host --user=$user --password=$passwd \
          --databases $database > tables.sql

### create the database with the command:
### $ mysql -p -u $user < database.sql


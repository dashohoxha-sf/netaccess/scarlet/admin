-- MySQL dump 10.9
--
-- Host: localhost    Database: netaccessadmin
-- ------------------------------------------------------
-- Server version	4.1.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `netaccessadmin`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `netaccessadmin` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `netaccessadmin`;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `username` varchar(20) NOT NULL default '',
  `firstname` varchar(100) default '',
  `lastname` varchar(100) default NULL,
  `e_mail` varchar(100) default NULL,
  `phone1` varchar(100) default NULL,
  `phone2` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `nr_routers` int(11) NOT NULL default '0',
  `expiration_time` datetime default '0000-00-00 00:00:00',
  `password` varchar(100) default '',
  PRIMARY KEY  (`username`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--


/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
LOCK TABLES `clients` WRITE;
INSERT INTO `clients` (`username`, `firstname`, `lastname`, `e_mail`, `phone1`, `phone2`, `address`, `notes`, `nr_routers`, `expiration_time`, `password`) VALUES ('test1','Test1','','','','','','',2,'2006-07-31 00:00:00','test1');
UNLOCK TABLES;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(10) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `charset` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--


/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
LOCK TABLES `languages` WRITE;
INSERT INTO `languages` (`id`, `name`, `charset`) VALUES ('en','English','iso-8859-1'),('sq_AL','Albanian','iso-8859-1'),('it','Italian','iso-8859-1'),('de','German','iso-8859-1'),('nl','Dutch','iso-8859-1');
UNLOCK TABLES;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `event` varchar(100) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--


/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
LOCK TABLES `logs` WRITE;
INSERT INTO `logs` (`id`, `time`, `event`, `details`) VALUES (1,'2006-07-29 14:58:08','+client','Source=admin, Admin=superuser, Client=test'),(2,'2006-07-29 14:59:59','~client','Source=admin, Admin=superuser, Client=test, Comment: 0 test1 2006-07-29 00:00'),(3,'2006-07-29 15:00:28','~client','Source=admin, Admin=superuser, Client=test, Comment: 0 test1 2006-08-29 00:00'),(4,'2006-07-29 15:00:54','~client','Source=admin, Admin=superuser, Client=test, Comment: 2 test1 2006-08-29 00:00'),(5,'2006-07-29 18:52:10','+ROUTER','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(6,'2006-07-29 18:52:24','+ROUTER','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(7,'2006-07-29 18:58:04','+ROUTER','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(8,'2006-07-29 18:58:08','+ROUTER','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(9,'2006-07-29 19:01:49','+ROUTER?','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(10,'2006-07-29 19:03:35','-Router','Source=admin, Admin=superuser, Client=test, Router=00:50:22:8c:7a:19, Comment: deletion'),(11,'2006-07-29 19:04:33','+ROUTER?','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: ISP Name router registration'),(12,'2006-07-29 19:12:07','+ROUTER?','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: \"ISP Name\" router registration'),(13,'2006-07-29 19:19:32','-Router','Source=admin, Admin=superuser, Client=test, Router=00:50:22:8c:7a:19, Comment: deletion'),(14,'2006-07-29 19:19:50','+ROUTER?','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: \"ISP Name\" router registration'),(15,'2006-07-29 19:19:50','+ROUTER!','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: \"ISP Name\" router registration'),(16,'2006-07-29 19:19:59','+ROUTER?','Source=client, Client=test, MAC=00:50:22:8c:7a:19, IP=127.0.0.1, Comment: \"ISP Name\" router registration'),(17,'2006-07-29 19:31:30','~ROUTER','Source=admin, Admin=superuser, MAC=00:50:22:8c:7a:19, Comment: modification'),(18,'2006-07-29 19:31:45','~ROUTER','Source=admin, Admin=superuser, MAC=00:50:22:8c:7a:19, Comment: modification'),(19,'2006-07-29 19:32:15','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(20,'2006-07-30 13:58:12','+ROUTER?','Source=client, Client=test2, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(21,'2006-07-30 13:58:43','+client','Source=admin, Admin=superuser, Client=test1'),(22,'2006-07-30 13:58:59','+ROUTER?','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(23,'2006-07-30 13:59:12','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(24,'2006-07-30 13:59:21','+ROUTER?','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(25,'2006-07-30 13:59:21','+ROUTER!','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(26,'2006-07-30 14:55:58','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(27,'2006-07-30 14:59:24','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(28,'2006-07-30 14:59:58','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(29,'2006-07-30 15:00:29','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(30,'2006-07-30 15:06:50','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(31,'2006-07-30 15:14:13','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(32,'2006-07-30 15:14:24','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(33,'2006-07-30 15:14:30','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(34,'2006-07-30 15:15:46','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(35,'2006-07-30 15:15:56','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(36,'2006-07-30 15:16:07','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(37,'2006-07-30 15:17:51','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(38,'2006-07-30 15:18:01','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(39,'2006-07-30 15:18:07','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(40,'2006-07-30 15:20:01','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(41,'2006-07-30 15:20:09','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(42,'2006-07-30 15:20:19','+ROUTER','Source=admin, Admin=superuser, MAC=, Comment: adding'),(43,'2006-07-30 15:20:24','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(44,'2006-07-30 15:20:31','-ROUTER','Source=check.php, Client=test1, MAC=jsjsjsj, Comment: expiration'),(45,'2006-07-30 15:20:31','-ROUTER','Source=check.php, Client=test1, MAC=hshshsh, Comment: expiration'),(46,'2006-07-30 15:23:28','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(47,'2006-07-30 15:25:25','-client','Source=admin, Admin=superuser, Client=test'),(48,'2006-07-30 15:28:54','-client','Source=admin, Admin=superuser, Client=test'),(49,'2006-07-30 15:29:35','+ROUTER?','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(50,'2006-07-30 15:29:35','+ROUTER!','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(51,'2006-07-30 17:23:34','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-29 00:00'),(52,'2006-07-30 17:26:02','-ROUTER','Source=check.php, Client=test1, MAC=52:54:00:12:34:56, Comment: expiration'),(53,'2006-07-30 17:27:28','~client','Source=admin, Admin=superuser, Client=test1, Comment: 2 test1 2006-07-31 00:00'),(54,'2006-07-30 17:27:40','+ROUTER?','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(55,'2006-07-30 17:27:40','+ROUTER!','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(56,'2006-07-30 17:28:57','-Router','Source=admin, Admin=superuser, Client=test1, Router=52:54:00:12:34:56, Comment: deletion'),(57,'2006-07-30 17:30:22','+ROUTER?','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration'),(58,'2006-07-30 17:30:22','+ROUTER!','Source=client, Client=test1, MAC=52:54:00:12:34:56, IP=192.168.0.250, Comment: \"ISP Name\" router registration');
UNLOCK TABLES;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

--
-- Table structure for table `routers`
--

DROP TABLE IF EXISTS `routers`;
CREATE TABLE `routers` (
  `client` varchar(30) NOT NULL default '',
  `mac` varchar(100) NOT NULL default '',
  `isp_name` varchar(100) NOT NULL default '',
  `public_ip` varchar(100) NOT NULL default '',
  `timestamp` varchar(100) default '',
  PRIMARY KEY  (`mac`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routers`
--


/*!40000 ALTER TABLE `routers` DISABLE KEYS */;
LOCK TABLES `routers` WRITE;
INSERT INTO `routers` (`client`, `mac`, `isp_name`, `public_ip`, `timestamp`) VALUES ('test1','52:54:00:12:34:56','ISP Name','192.168.0.250','1154273422');
UNLOCK TABLES;
/*!40000 ALTER TABLE `routers` ENABLE KEYS */;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session`
--


/*!40000 ALTER TABLE `session` DISABLE KEYS */;
LOCK TABLES `session` WRITE;
INSERT INTO `session` (`id`, `vars`) VALUES ('IP: 192.168.1.11; DATE: 2006-07-28 15:40:26','a:0:{}'),('IP: 192.168.1.11; DATE: 2006-07-28 15:40:35','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 192.168.1.111; DATE: 2006-07-28 16:01:23','a:0:{}'),('IP: 192.168.1.111; DATE: 2006-07-28 16:01:31','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 192.168.1.11; DATE: 2006-07-28 19:47:08','a:0:{}'),('IP: 192.168.1.11; DATE: 2006-07-28 19:47:15','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 127.0.0.1; DATE: 2006-07-29 09:31:01','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 09:31:08','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 09:31:18','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 127.0.0.1; DATE: 2006-07-29 13:30:24','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 13:31:52','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 13:45:27','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 13:45:46','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 13:54:45','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:14:53','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:57:27','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:58:32','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:59:26','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:59:34','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 127.0.0.1; DATE: 2006-07-29 14:59:45','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 15:00:07','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 15:00:39','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 15:01:01','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 15:03:33','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:53:27','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:53:36','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:53:59','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:54:09','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:54:49','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:55:14','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 17:55:36','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:49:09','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:49:53','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:51:24','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:52:10','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:52:24','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:58:04','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 18:58:08','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 19:01:49','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 19:04:33','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 19:12:07','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 19:19:50','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-29 19:19:59','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 13:53:55','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-30 13:54:20','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-30 13:54:27','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}'),('IP: 192.168.0.250; DATE: 2006-07-30 13:57:40','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 13:58:12','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 13:58:59','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 13:59:21','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 14:00:20','a:0:{}'),('IP: ; DATE: 2006-07-30 14:55:01','a:0:{}'),('IP: ; DATE: 2006-07-30 14:56:07','a:0:{}'),('IP: ; DATE: 2006-07-30 15:05:16','a:0:{}'),('IP: ; DATE: 2006-07-30 15:06:12','a:0:{}'),('IP: ; DATE: 2006-07-30 15:06:24','a:0:{}'),('IP: ; DATE: 2006-07-30 15:07:02','a:0:{}'),('IP: ; DATE: 2006-07-30 15:14:35','a:0:{}'),('IP: ; DATE: 2006-07-30 15:16:23','a:0:{}'),('IP: ; DATE: 2006-07-30 15:18:12','a:0:{}'),('IP: ; DATE: 2006-07-30 15:20:31','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 15:22:43','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 15:29:35','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 17:25:30','a:0:{}'),('IP: ; DATE: 2006-07-30 17:26:02','a:0:{}'),('IP: ; DATE: 2006-07-30 17:26:22','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 17:26:52','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 17:27:40','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 17:29:51','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 17:30:22','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 19:19:09','a:0:{}'),('IP: 192.168.0.250; DATE: 2006-07-30 19:20:34','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-31 09:07:00','a:0:{}'),('IP: 127.0.0.1; DATE: 2006-07-31 09:07:08','a:3:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:34:\"$1$I1rX7MdT$JF.ueqZplWKrYHJrt.bIx.\";s:4:\"u_id\";s:1:\"0\";}');
UNLOCK TABLES;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(20) default NULL,
  `password` varchar(20) NOT NULL default '',
  `firstname` varchar(100) NOT NULL default '',
  `lastname` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `phone1` varchar(100) default NULL,
  `phone2` varchar(100) default NULL,
  `e_mail` varchar(100) default NULL,
  `notes` text,
  `access_rights` text,
  PRIMARY KEY  (`user_id`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--


/*!40000 ALTER TABLE `users` DISABLE KEYS */;
LOCK TABLES `users` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * @package clients
 */
class router_edit extends WebObject
{
  var $router_record = array(
                             'mac'       => '',
                             'public_ip' => '',
                             'isp_name'  => '',
                             'timestamp' => ''
                             );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('mac', UNDEFINED);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      $event_args['client'] = WebApp::getSVar('clientList->current_client');
      $event_args['timestamp'] = time();

      if ($mode=='add')
        {
          $this->add_router($event_args);
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('update_router', $event_args);
          $this->setSVar('mode', 'hidden');

          //log the event
          $user = WebApp::getSVar('username');
          $mac = $event_args['mac'];
          $d = "Source=admin, Admin=$user, MAC=$mac, Comment: modification";
          log_event('~ROUTER', $d);
        }

    }

  function add_router($record)
    {
      //check that router does not exist in the table
      $rs = WebApp::openRS('get_router', $record);
      if (!$rs->EOF())
        {
          $this->router_record = $record;
          $mac = $record['mac'];
          $msg = T_("MAC 'v_mac' is already used.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return;
        }

      //allow this ROUTER in the firewall
      $path = APP_PATH."server-config/firewall";
      $router = $record['router'];
      shell("$path/router-allow.sh $router");

      //add the new router
      WebApp::execDBCmd('add_router', $record);

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');

      //log the event
      $user = WebApp::getSVar('username');
      $d = "Source=admin, Admin=$user, MAC=$MAC, Comment: adding";
      log_event('+ROUTER', $d);
    }

  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          WebApp::addVars($this->router_record);
        }
      else if ($mode=='edit')
        {
          $args = array('mac' => $this->getSVar('mac'));
          $rs = WebApp::openRS('get_router', $args);
          $vars = $rs->Fields();
          WebApp::addVars($vars);
        }
    }
}
?>
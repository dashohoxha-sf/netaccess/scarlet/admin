<?php
/**
 * Is called with a query string like this: ?querystring
 * This query string contains a serialized array, which contains
 * registration data, like the client username and registration code,
 * the mac of the router being registered, the name of the ISP, etc.
 * It checks that the client username and password are valid and then
 * makes the registration of the router, recording the time of registration,
 * the mac of the router, the (public) IP from which comes the request,
 * and the name of ISP. Finally returns 'OK' to indicate that the
 * registration was done successfully, or NOK and an error message,
 * if the registration failed for some reason.
 */

//get the registration data from the query string
$data = $_SERVER["QUERY_STRING"];
$data = trim(rawurldecode($data));
$arr_data = unserialize($data);

//get the variables
$username = $arr_data['username'];
$password = $arr_data['password'];
$mac = $arr_data['mac'];
$isp = $arr_data['isp'];
$lng = $arr_data['lng'];
$codeset = $arr_data['codeset'];
$public_ip = $_SERVER['REMOTE_ADDR'];

//include the framework classes and utilities (override the language settings)
define('LNG', $lng);
define('CODESET', $codeset);
include_once "webapp.php";

//log the event
$details = ("Source=client, Client=$username, MAC=$mac, "
            . "IP=$public_ip, Comment: \"$isp\" router registration");
log_event('+ROUTER?', $details);

//get the data of the client
$query = ("SELECT username, password, nr_routers, expiration_time"
          . " FROM clients"
          . " WHERE username = '$username' AND password = '$password'");
$client_rs = WebApp::execQuery($query);

//check that the user (with the given username and password) does exist
if ($client_rs->EOF())  //no client with such a username and password
{
  $msg = T_("There is no client with such a username and password! \
Please try again.");
  reply('NOK', $msg);
}

//check that the expiration time has not passed
$expiration_time = $client_rs->Field('expiration_time');
if (strtotime($expiration_time) < time())
{
  $msg = T_("The registration time limit has expired, please renew it.");
  reply('NOK', $msg);
}

//check that this router is not already registered
$query = "SELECT mac FROM routers WHERE client = '$username'";
$mac_rs = WebApp::execQuery($query);
$arr_macs = $mac_rs->getColumn('mac');
if (in_array($mac, $arr_macs))
{
  reply('NOK', T_("This router seems to be already registered."));
}

//check that the max nr of routers is not passed
$nr_routers = $client_rs->Field('nr_routers');
if ($mac_rs->count >= $nr_routers)
{
  $msg = T_("You can register up to var_nr_routers routers \
and this number has been reached.");
  $msg = str_replace('var_nr_routers', $nr_routers, $msg);
  reply('NOK', $msg);
}

//register this router for the client
$timestamp = time();
$query = "
INSERT INTO routers
SET
  client    = '$username',
  mac       = '$mac',
  isp_name  = '$isp',
  public_ip = '$public_ip',
  timestamp = '$timestamp'
";
WebApp::execQuery($query);

//log the event
$details = ("Source=client, Client=$username, MAC=$mac, "
            . "IP=$public_ip, Comment: \"$isp\" router registration");
log_event('+ROUTER!', $details);

//send the reply
reply('OK', T_("Router registered successfully."));

exit;

/*----------------------- functions -----------------------------*/

/**
 * Send a reply to the registration request and exit.
 * $code can be 'OK' or 'NOK' and the message is a message that is
 * displayed to the client (e.g. to explain the type of error).
 */
function reply($code, $message)
{
  print $code."\n".$message;
  exit;
}
?>
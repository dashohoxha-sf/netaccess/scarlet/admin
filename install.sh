#!/bin/bash
### install the application

### go to this dir
cd $(dirname $0)

### get the application path
app_path=$(dirname $(pwd))

### make executable all the script files
find . -name '*.sh' | xargs chmod +x

### compile wrapper.c and set proper ownership/permissions
gcc -o wrapper wrapper.c
chown root:root wrapper
chmod +s wrapper

### init the database
db/init.sh

### compile the translation files
langs="en sq_AL nl it de"
for lng in $langs ; do l10n/msgfmt.sh $lng ; done

### set the superuser password
scripts/su_passwd.sh

### create the script that runs 'check.php' daily
file=/etc/cron.daily/netaccessadmin_check.sh
cat <<EOF > $file
#!/bin/bash
$app_path/check.php
EOF
chmod +x $file

